FROM adoptopenjdk/openjdk15:alpine-jre
WORKDIR /opt/app
COPY target/demo-rest-api-0.0.1-SNAPSHOT.jar demorestapi.jar
EXPOSE 8080
ENTRYPOINT ["java","-jar","demorestapi.jar"]
