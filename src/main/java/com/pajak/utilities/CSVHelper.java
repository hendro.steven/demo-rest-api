package com.pajak.utilities;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import com.pajak.models.entities.Author;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
import org.springframework.web.multipart.MultipartFile;

public class CSVHelper {

    public static String TYPE = "text/csv";
    
    public static boolean hasCSVFormat(MultipartFile file){
        if(!TYPE.equals(file.getContentType())){
            return false;
        }
        return true;
    }

    public static List<Author> csvToAuthors(InputStream is){
        try{
            List<Author> authors = new ArrayList<>();
            
            BufferedReader fileReader = new BufferedReader(new InputStreamReader(is, "UTF-8"));

            CSVParser csvParser = new CSVParser(fileReader, 
                CSVFormat.DEFAULT.withFirstRecordAsHeader().withIgnoreHeaderCase().withTrim());

            Iterable<CSVRecord> records = csvParser.getRecords();

            for(CSVRecord record : records){
                Author author = new Author();
                author.setFullName(record.get("FullName"));
                author.setEmail(record.get("Email"));
                authors.add(author);
            }

            csvParser.close();
            return authors;

        }catch(Exception e){
            throw new RuntimeException("fail to parse the CSV file: "+e.getMessage());
        }
    }
}
