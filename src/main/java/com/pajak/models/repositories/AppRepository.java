package com.pajak.models.repositories;

import com.pajak.models.entities.App;

import org.springframework.data.repository.CrudRepository;

public interface AppRepository extends CrudRepository<App, Long> {
    
    public App findByUserName(String userName);
}
