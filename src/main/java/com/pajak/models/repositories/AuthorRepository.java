package com.pajak.models.repositories;

import com.pajak.models.entities.Author;

import org.springframework.data.jpa.repository.JpaRepository;

public interface AuthorRepository extends JpaRepository<Author, String> {
    
}
