package com.pajak.models.repositories;

import java.util.List;

import com.pajak.models.entities.Sessions;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

public interface SessionRepository extends CrudRepository<Sessions, Long> {
    
    public Sessions findBySessionId(String sessionId);

    public List<Sessions> findByUserId(Long userId);

    @Modifying
    @Query("UPDATE Sessions s SET s.active = :param1 WHERE s.user.id = :param2")
    public int setActiveByUserId(@Param("param1") boolean active,
        @Param("param2") Long userId);
}
