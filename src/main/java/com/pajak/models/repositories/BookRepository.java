package com.pajak.models.repositories;

import java.util.List;

import com.pajak.models.entities.Book;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;

public interface BookRepository extends PagingAndSortingRepository<Book, Long> {
    
    @Query("SELECT b FROM Book b WHERE b.title = :param")
    public List<Book> cariBerdasarkanTitle(@Param("param") String title);

    public List<Book> findByTitle(String title);

    public List<Book> findByTitleAndAuthor(String title, String author);

    public List<Book> findByTitleContains(String title);

    public List<Book> findByCategoryId(Long id);
}
