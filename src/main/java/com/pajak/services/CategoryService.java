package com.pajak.services;

import javax.transaction.Transactional;

import com.pajak.models.entities.Category;
import com.pajak.models.repositories.CategoryRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@Transactional
public class CategoryService {
    
    @Autowired
    private CategoryRepository categoryRepository;

    public Category save(Category category){
        return categoryRepository.save(category);
    }

    public Category update(Long id, Category editedCategory){
        Category category = categoryRepository.findById(id).get();
        category.setName(editedCategory.getName());
        return categoryRepository.save(category);
    }

    public Category findOne(Long id){
        return categoryRepository.findById(id).get();
    }

    public void remove(Long id){
        categoryRepository.deleteById(id);
    }
   
}
