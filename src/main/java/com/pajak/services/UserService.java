package com.pajak.services;

import java.util.Date;

import javax.transaction.Transactional;

import com.pajak.models.entities.Sessions;
import com.pajak.models.entities.User;
import com.pajak.models.repositories.SessionRepository;
import com.pajak.models.repositories.UserRepository;
import com.pajak.utilities.MD5Generator;
import com.pajak.utilities.UUIDGenerator;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@Transactional
public class UserService {
    
    @Autowired
    private UserRepository userRepository;

    @Autowired
    private SessionRepository sessionRepository;

    public User save(User user) throws Exception {
        user.setPassword(MD5Generator.generate(user.getPassword()));
        return userRepository.save(user);
    }

    public User findById(Long id){
        return userRepository.findById(id).get();
    }

    public Iterable<User> findAll(){
        return userRepository.findAll();
    }

    public User findByEmailAndPassword(String email, String password){

        User user = null;
        try{
            user = userRepository
                    .findByEmailAndPassword(email, MD5Generator.generate(password));
            return user;
        }catch(Exception ex){
            ex.printStackTrace();
            return null;
        }
    
    }

    public Sessions login(String email, String password){
        User user = findByEmailAndPassword(email, password);
        if(user == null){
            return null;
        }
        try{
            //kick all sessionId sebelumnya
            sessionRepository.setActiveByUserId(false, user.getId());

            //create new Session
            Sessions session = new Sessions();
            session.setSessionId(UUIDGenerator.generateUniqueKeysWithUUIDAndMessageDigest());
            session.setUser(user);
            session.setActive(true);
            return sessionRepository.save(session);
        }catch (Exception ex) {
            ex.printStackTrace();
            return null;
        }
    }

    public Sessions logout(String sessionId){
        Sessions session = sessionRepository.findBySessionId(sessionId);
        if(session != null){
            session.setLogoutAt(new Date());
            session.setActive(false);
            return sessionRepository.save(session);
        }else{
            return null;
        }
    }
}
