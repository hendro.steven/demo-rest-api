package com.pajak.controllers;

import javax.servlet.http.HttpServletRequest;

import com.pajak.dto.ResponseData;
import com.pajak.models.entities.Sessions;
import com.pajak.services.UserService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/modules/users/logout")
public class LogoutUserController {
    
    @Autowired
    private UserService userService;

    @Autowired
    private HttpServletRequest request;

    @GetMapping
    public ResponseEntity<ResponseData> logout(){
        ResponseData response = new ResponseData();
        try{
            Sessions session = userService
                .logout(request.getHeader("SESSIONID"));
            response.setPayload(session);
            response.setStatus(true);
            response.getMessages().add("User logout");
            return ResponseEntity.ok(response);
        }catch(Exception ex){
            response.setStatus(false);
            response.getMessages().add(ex.getMessage());
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(response);
        }
    }
}
