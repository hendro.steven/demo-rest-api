package com.pajak.controllers;

import java.util.Base64;

import com.pajak.dto.ResponseData;
import com.pajak.models.entities.App;
import com.pajak.services.AppService;
import com.pajak.utilities.UUIDGenerator;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/apps")
public class AppController {

    @Autowired
    private AppService appService;

    @PostMapping
    public ResponseEntity<ResponseData> registerApp(@RequestBody App app) {
        ResponseData response = new ResponseData();
        try {
            app.setPassword(
                UUIDGenerator.generateUniqueKeysWithUUIDAndMessageDigest());
            app.setAppKey(
                generateAppKey(app.getUserName(), app.getPassword()));
            app = appService.register(app);
            response.setStatus(true);
            response.getMessages().add("App saved");
            response.setPayload(app);
            return ResponseEntity.ok(response);
        } catch (Exception ex) {
            response.setStatus(false);
            response.getMessages().add(ex.getMessage());
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(response);
        }
    }

    @GetMapping
    public ResponseEntity<ResponseData> findAll(){
        ResponseData response = new ResponseData();
        response.setStatus(true);
        response.setPayload(appService.findAll());
        return ResponseEntity.ok(response);
    }

    private String generateAppKey(String userName, String password) {
        String baseStr = userName + ":" + password;
        String appKey = Base64.getEncoder().encodeToString(baseStr.getBytes());
        return appKey;
    }
}
