package com.pajak.controllers;

import java.util.List;

import com.pajak.dto.ResponseData;
import com.pajak.models.entities.Author;
import com.pajak.models.repositories.AuthorRepository;
import com.pajak.services.AuthorService;
import com.pajak.utilities.CSVHelper;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

@RestController
@RequestMapping("/api/modules/authors")
public class AuthorController {
    
    @Autowired
    private AuthorRepository authorRepository;

    @Autowired
    private AuthorService authorService;
    
    @PostMapping
    public Author create(@RequestBody Author author){
        return authorRepository.save(author);
    }

    @GetMapping
    public Iterable<Author> getAll(){
        return authorRepository.findAll();
    }

    @PostMapping("/upload")
    public ResponseEntity<ResponseData> uploadFile(@RequestParam("file") MultipartFile file){

        ResponseData response = new ResponseData();
        
        if(CSVHelper.hasCSVFormat(file)){
            try{
                List<Author> authors = authorService.save(file);
                response.setStatus(true);
                response.setPayload(authors);
                response.getMessages().add("Upload success");
                return ResponseEntity.ok(response);
            }catch(Exception ex){
                response.setStatus(false);
                response.getMessages().add(ex.getMessage());
                return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(response);
            }
        }

        response.setStatus(false);
        response.getMessages().add("Please upload a CSV file!");
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(response);
    }
}
