package com.pajak;

import static org.junit.jupiter.api.Assertions.assertNotNull;

import com.pajak.models.entities.Category;
import com.pajak.services.CategoryService;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class DemoRestApiApplicationTests {

	@Autowired
	private CategoryService categoryService;

	@Test
	void categoryTest() {
		Category category = new Category();
		category.setName("CategoryTest");
		category = categoryService.save(category);
		assertNotNull(category.getId());

		categoryService.remove(category.getId());
	}

}
